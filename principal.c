#include <stdio.h>
#include <stdlib.h>
#include "tablaSimbolos.h"
#include "definiciones.h"
#include "lex.yy.h"

int main(int argc, char** argv) {

	componenteLexico comp;
    	int i = 0;
    	FILE *file = fopen("wilcoxon.py", "rt");
	cargarArchivo(file);
    	crearTabla(32);
   	do {
   	     siguienteComponente(&comp);
   	     i++;
   	     fflush(stdout);
	} while ((comp.id) != -1);
    	imprimir();
    	fclose(file);
    
    	return (EXIT_SUCCESS);

}
