%{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tablaSimbolos.h"
#include "definiciones.h"
%}

/* Definiciones */

IGUAL [=]
IGUALDOBLE [=]{2}
INCREMENTO [+][=]
DECREMENTO [-][=]
ESPACIO [{ blank: }]
SALTODELINEA [\n]
DELIMITADORES [;,-<>=*().\/{}:+\[\]]
COMENTARIO [#][^\n]
COMENTARIOLARGO \"\"\"[^\"\"\"]*\"\"\"
CADENA \"[^"]*\"|\'[^']*\'
EXPONENTE [eE][+-][0-9]*
IDENTIFICADOR [a-zA-Z_][a-zA-Z0-9_]*
ENTEROS [0-9]+|0[xX][0-9]+
REALES [0-9]+"."[0-9]+|"."[0-9]+|[0-9]+{EXPONENTE}|[0-9]+"."[0-9]+{EXPONENTE}

%%

{IGUAL}   {
    return EQUALS;
}
{IGUALDOBLE}   {
    return PLUSPLUS;
}
{INCREMENTO}   {
    return MORE_EQUALS;
}
{DECREMENTO}   {
    return MINUS_EQUALS;
}
{ESPACIO}   {
    return BLANK;
}
{SALTODELINEA}   {
    return ENTER;
}
{DELIMITADORES}   {
    return LIMITS;
}
{COMENTARIO}   {
    return COMMENT;
}
{CADENA}   {
    return STRING;
}
{EXPONENTE}   {
    return EXP;
}
{IDENTIFICADOR}   {

	insertarEnTabla(yytext);
    	return IDENTIFIER;
}
{ENTEROS}   {
    return INTEGER;
}
{REALES}   {
    return FLOAT;
}

<<EOF>>       return -1;

%%


void cargarArchivo(FILE *file) {
	yyin = file;
}


void siguienteComponente(componenteLexico *s) {
  s->id = yylex();
  s->lexema = (char*) malloc(sizeof(char) * yyleng);
  strncpy(s->lexema, yytext, yyleng);
  s->lexema[yyleng] = '\0';
}








