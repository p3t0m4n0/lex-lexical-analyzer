#include <stdio.h>
#include <time.h>
#include <string.h>
#include <stdlib.h>
#include <string.h>
#include "definiciones.h"
#include "tablaHash.h"

static hashtable_t *tablaHash;

void crearTabla(int tamanho) {
    tablaHash = ht_create(tamanho);
    ht_set(tablaHash, "import", IMPORT);
    ht_set(tablaHash, "def", DEF);
    ht_set(tablaHash, "for", FOR);
    ht_set(tablaHash, "in", IN);
    ht_set(tablaHash, "if", IF);
    ht_set(tablaHash, "elif", ELIF);
    ht_set(tablaHash, "else", ELSE);
    ht_set(tablaHash, "return", RETURN);
}

int buscarEnTabla(char lexema []) {
    return ht_get(tablaHash, lexema);
}

void imprimir() {
    ht_print(tablaHash);
}

void insertarEnTabla(char lexema []) {
	if (!buscarEnTabla(lexema)) {
        	ht_set(tablaHash, lexema, 400);
   	}   
}
